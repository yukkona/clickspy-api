chAdapter = ClickAdapter.new()

configure do
  enable :cross_origin
end

namespace '/api/v1' do
  before do
    content_type 'application/json'
  end

  get '/' do
    'Welcome to ClickSpy API'
  end

  namespace '/user' do
    get '/' do
      'User root endpoint reached'
    end
  end

  namespace '/domain' do
    get '/' do
      'Domain root endpoint reached'
    end
    get '/add' do
      chAdapter.registerDomain(request.params["schema"], request.params["domain"]).to_json # В доменных именах не рекомендуется использовать "_" (это не смайлик)
    end
    get '/list' do
      chAdapter.listDomains().to_json
    end
    namespace '/:id' do |id|
      namespace '/click' do
        get '/new' do |id|
          chAdapter.registerClick(id, request.params["x"], request.params["y"], request.params["hour"], request.params["page"])
          f = Screencap::Fetcher.new("")
          screenshot = f.fetch
        end
        namespace '/stat' do
          get '/by-hour' do |id|
            chAdapter.statByHour(id).to_json
          end
          get '/heat-map' do |id|
            chAdapter.heatMap(id).to_json
          end
        end
      end
    end
  end
end
