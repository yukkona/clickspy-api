require 'clickhouse'
require 'digest'
require 'logger'
require 'yaml'

class ClickAdapter
  def initialize
    config = YAML.load_file("./config/clickhouse.yml")[YAML.load_file("./config/app.yml")["environment"]]
    Clickhouse.logger = Logger.new(STDOUT)
    Clickhouse.establish_connection(urls: config[:urls], username: config[:user], password: config[:password], database: config[:db]) # Пароль плейнтекстом, найти обход
  end

  def listDomains()
    stat = []
    Clickhouse.connection.select_rows(:select => "schema, domain, token", :from => 'domains').each do |row|
      stat << { "address" => "#{row[0]}://#{row[1]}", "token" => row[2], "domain" => row[1] }
    end
    return stat
  end

  def hasDomain?(domain)
    val = false
    listDomains.each do |table|
      if table["domain"] == domain
        val = true
        break
      end
    end
    return val
  end

  def registerDomain(schema, domain)
    val = {}
    if hasDomain?(domain) == true
      val["result"] = false
      val["message"] = "Domain already registered"
    else
      token = generateToken()
      cdb = Clickhouse.connection.create_table(token) do |t|
        t.uint8   :x
        t.uint8   :y
        t.uint8   :hour
        t.string  :page
        t.engine  "Memory"
      end
      val["result"] = cdb
      if cdb == true
        val["message"] = "Success"
        val["data"] = { "token" => token }
        Clickhouse.connection.insert_rows('domains', :names => %w(schema domain token)) do |rows|
          rows << [schema, domain, token]
        end
      else
        val["message"] = "Query error"
      end
    end
    return val
  end

  def registerClick(token, x, y, hour, page)
    Clickhouse.connection.insert_rows(token, :names => %w(x y hour page)) do |rows|
      rows << [x, y, hour, page]
    end
  end

  def statByHour(token)
    stat = []
    Clickhouse.connection.select_rows(:select => "hour, count() as clicks", :from => token, :group => "hour").each do |row|
      stat << { "hour" => row[0], "clicks" => row[1] }
    end
    return stat
  end

  def heatMap(token)
    stat = []
    Clickhouse.connection.select_rows(:select => "x, y, count() as count", :from => token, :group => "x, y").each do |row|
      stat << { "x" => row[0], "y" => row[1], "count" => row[2] }
    end
    return stat
  end

  private
  def generateToken()
    return `head -c 1000 /dev/urandom | tr -dc 'A-Z' | fold -w 16 | head -n 1`
  end
end