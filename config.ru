require 'require_all'
require 'yaml'
require 'sinatra'
require "sinatra/namespace"
require_all "./app"

require "./config/router.rb"

FileUtils.mkdir_p 'log' unless File.exists?('log')
FileUtils.mkdir_p 'tmp' unless File.exists?('tmp')

log = File.new("log/app.log", "a+")

$stdout.reopen(log)
$stderr.reopen(log)

run Sinatra::Application